<?php
/*
 * @file
 * This file contains the menu callbacks for the admin configuration page
 */
function limesurvey_integeration_admin_settings() {
  $fieldnames = array();
  $fieldnames['username'] = 'Username';
  $fieldnames['email'] = 'E-Mail';
  foreach (field_info_instances('user', 'user') as $key => $value) {
      $fieldnames[$key] = $value['label'] . '  (' . $value['field_name'] . ')';
  }

  $form = array();

  $form['lsintegeration_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable the sync'),
    '#default_value' => variable_get('lsintegeration_enabled', 1),
    '#description' => t('Enable the module'),
    '#required' => TRUE,
  );

  $form['lsintegeration_lsurl'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter the limesurvey URL with which you want to sync'),
    '#default_value' => variable_get('lsintegeration_lsurl', ''),
    '#description' => t('Please enter the url with which you want to sync your drupal contacts (upto admin with a trailing slash)'),
    '#size' => '100'
  );
  
  $form['lsintegeration_surveyid'] = array(
    '#type' => 'textfield',
    '#title' => t('The Survey ID with which we want to sync'),
    '#default_value' => variable_get('lsintegeration_surveyid', ''),
    '#size' => '6',
    '#maxlength' => '6'
  );
  $form['lsintegeration_username'] = array(
    '#type' => 'textfield',
    '#title' => t('User Name for Limesurvey'),
    '#default_value' => variable_get('lsintegeration_username', ''),
    '#size' => '30',
    '#maxlength' => '50'
  );
  
  $form['lsintegeration_password'] = array(
    '#type' => 'password',
    '#title' => t('Password for Limesurvey'),
    '#default_value' => variable_get('lsintegeration_password', ''),
    '#size' => '30',
    '#maxlength' => '50'
  );
  
  $form['lsintegeration_fname'] = array(
    '#type' => 'select',
    '#title' => t('LS Integeration Field Mapping for First Name'),
    '#options' => $fieldnames,
    '#default_value' => variable_get('lsintegeration_fname', '')
  );
  
  $form['lsintegeration_lname'] = array(
    '#type' => 'select',
    '#title' => t('LS Integeration Field Mapping for Last Name'),
    '#options' => $fieldnames,
    '#default_value' => variable_get('lsintegeration_lname', '')
  );
  
  $form['lsintegeration_email'] = array(
    '#type' => 'select',
    '#title' => t('LS Integeration Field Mapping for E-Mail'),
    '#options' => $fieldnames,
    '#default_value' => variable_get('lsintegeration_email', 'email')
  );
  
  $form['lsintegeration_vfrom'] = array(
    '#type' => 'date',
    '#title' => t('LS Integeration Field for valid from for participants'),
    '#default_value' => variable_get('lsintegeration_vfrom', '')
  );
  
  $form['lsintegeration_vuntil'] = array(
    '#type' => 'date',
    '#title' => t('LS Integeration Field for valid until for participants'),
    '#default_value' => variable_get('lsintegeration_vuntil', array('month' => 4, 'day' => 5, 'year' => 2012))
  );
  
  $options_black = array();
  $options_black['Y'] = 'Yes';
  $options_black['N'] = 'No';
  
  $form['lsintegeration_blacklist'] = array(
      '#type' => 'select',
      '#title' => t('What should be blacklist field for Limesurvey'),
      '#default_value' => variable_get('lsintegeration_blacklist', 'N'),
      '#options' => $options_black
  );
  
  $form['lsintegeration_language'] = array(
      '#type' => 'textfield',
      '#title' => t('Langauge for Limesurvey'),
      '#default_value' => variable_get('lsintegeration_language', 'en'),
      '#size' => '2',
      '#maxlength' => '2'
  );
  
  $form['lsintegeration_emailstatus'] = array(
      '#type' => 'textfield',
      '#title' => t('E-Mail status for Limesurvey'),
      '#default_value' => variable_get('lsintegeration_emailstatus', 'Ok'),
      '#size' => '2',
      '#maxlength' => '2'
  );
  
  return system_settings_form($form);
  
}
function limesurvey_integeration_status() {
  $header = array(
        array('data' => t('User ID'), 'field' => 'uid', 'sort' => 'asc'),
        array('data' => t('First Name'), 'field' => 'name', 'sort' => 'asc'),
        array('data' => t('Last Name'), 'field' => 'name', 'sort' => 'asc'),
        array('data' => t('E-Mail'), 'field' => 'mail', 'sort' => 'asc'),
        array('data' => t('Status'), 'field' => 'status', 'sort' => 'asc'),
  );
  $users = array();
  $query = db_select('users', 'u')
           ->condition('u.uid', 0, '<>')
           ->condition('u.uid', 1, '!=')
           ->condition('u.uid', 0, '!=')
           ->extend('PagerDefault')
           ->limit(30)
           ->extend('TableSort')        //Sorting Extender
           ->orderByHeader($header)
           ->fields('u', array('uid', 'mail', 'name', 'status'));
  $result = $query->execute();
  $rows = array();
  foreach ($result->fetchAll() as $value) {
    if (variable_get('lsintegeration_fname', '') == 'username') {
      $firstname =  $value->name;
    }
    else {
      $fieldname = variable_get('lsintegeration_fname', '');
      $query  = db_select("field_data_$fieldname", 'fname');
      $query  -> condition('fname.entity_id', $value->uid, '=')
              -> fields('fname', array($fieldname . "_value"));
      $result = $query->execute();
      $firstname = $result->fetchColumn();
    }
    
    if (variable_get('lsintegeration_lname', '') == 'username') {
        $lastname =  $value->name;
    }
    else {
      $fieldname = variable_get('lsintegeration_lname', '');
      $query  =  db_select("field_data_$fieldname", 'lname');
      $query  -> condition('lname.entity_id', $value->uid, '=')
              -> fields('lname', array($fieldname . '_value'));
      $result = $query->execute();
      $lastname = $result->fetchColumn();
    }

    if (variable_get('lsintegeration_email', '') == 'email') {
        $email =  $value->mail;
    }
    else {
        $fieldname = variable_get('lsintegeration_email', '');
        $query  = db_select("field_data_$fieldname", 'lname');
        $query  ->condition('lname.entity_id', $value->uid, '=')
                ->fields('lname', array($fieldname . '_value'));
        $result = $query->execute();
        $email = $result->fetchColumn();
    }

    $query = db_select('limesurvey_users', 'lu');
    $query->condition('lu.uid', $value->uid, '=')
          ->fields('lu', array('status'));
    $result = $query->execute();
    $status = $result->fetchColumn();
    $finalstatus = '';
      
    if ($status == 1) {
      $finalstatus = "<p style='color:green;'>" . t('In LimeSurvey') . "</p> ";
    }
    elseif ($status == 2) {
      $finalstatus =  "<p style='color:red;'>" . t('Error: Invalid survey ID') . "</p> ";
    }
    elseif ($status == 3) {
      $finalstatus = "<p style='color:red;'>" . t('Invalid session key') . "</p>";
    }
    elseif ($status == 4) {
      $finalstatus = "<p style='color:red;'>" . t('No permission') . "</p> ";
    }
    else {
      $finalstatus = "<p style='color:red;'>" . t('Not In LimeSurvey') . "</p> ";
    }
    $rows[] = array( 
        'data' => array(
            $value->uid,
            $firstname,
            $lastname,
            $email,
            $finalstatus
        )
    );
  }

  $build['pager_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('There are no users in the user table'),
  );
  
  $build['pager_pager'] = array('#theme' => 'pager');
  return $build;
}
function limesurvey_integeration_clearsync_form_submit($form, &$form_state) {
  $result = db_truncate('limesurvey_users')->execute();
}
function limesurvey_integeration_clearsync_form($form, &$form_state) {
  $form['deletion_warning'] = array(
  '#type' => 'item',
  '#markup' => "<p style='color:red;'>WARNING !!! This will delete any syncing with the previous survey you had 
                and the module will again resync all the users to the survey that is set in the
                configuration page</p>"
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Clear Sync'),
  );
  return $form;
}
