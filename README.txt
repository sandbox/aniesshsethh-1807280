This Module allows the user to sync the users from the Drupal front end website to the LimeSurvey 
(http://www.limesurvey.org/) survey taking web application. As soon as a user is registered on the drupal website, 
the user can configure it's limesurvey installation at admin/config/lsintegeration/config and test the settings by 
clicking save configuration, if the configurations are not correct a error will be show , else on every cron run the 
user information will start going to the Limesurvey Installation using it's API.

The user can define the mapping between drupal and default limesurvey survey table, and the users are added to the 
particular survey id which is defined.

To take care of heavy traffic websites, the process is added a queue and hence is better managed than trying to sync 
all the users at one go.

The administrator at the limesurvey end can then invite those users to participate in a particular survey.